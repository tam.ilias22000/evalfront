import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FilmCardComponent } from './components/film-card/film-card.component';
import { FilmListComponent } from './pages/film-list/film-list.component';
import { FilmByIdComponent } from './pages/film-by-id/film-by-id.component';
import { CreationComponent } from './pages/creation/creation.component';
import { EditionComponent } from './pages/edition/edition.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import {MatFormFieldModule} from '@angular/material/form-field';



@NgModule({
  declarations: [
    AppComponent,
    FilmCardComponent,
    FilmListComponent,
    FilmByIdComponent,
    CreationComponent,
    EditionComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
   
    RouterModule.forRoot([
      {path:'',component:FilmListComponent},
      {path:'films/:id', component:FilmByIdComponent},
      {path:'new', component:CreationComponent},
      {path:'update/:id', component:EditionComponent}
    ]),
   
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
