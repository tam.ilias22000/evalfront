import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Film } from '../models/Film';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(private httpClient:HttpClient) { }

  findAll(){
    return this.httpClient.get<Film[]>('http://localhost:3000/films')
  }

  findById(id:number){
    return this.httpClient.get<Film>(`http://localhost:3000/films/${id}`)
  }

  save(film:Film){
    return this.httpClient.post<Film>('http://localhost:3000/films', film)
  }

  update(id:number, film:Film){
    return this.httpClient.put<Film>(`http://localhost:3000/films/${id}`, film)
  }

  delete(id:number){
    return this.httpClient.delete(`http://localhost:3000/films/${id}`)
  }
}
