import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Film } from 'src/app/models/Film';
import { FilmService } from 'src/app/services/film.service';

@Component({
  selector: 'app-edition',
  templateUrl: './edition.component.html',
  styleUrls: ['./edition.component.css']
})
export class EditionComponent implements OnInit {

  film!:Film
  id!:number
  constructor(private filmService:FilmService, private router:Router, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.film = new Film();
    
    this.id = this.route.snapshot.params['id'];
    
    this.filmService.findById(this.id).subscribe(res => {
      this.film = res;
    } ,error => console.log(error))
  }

  updateFilm(){
    this.filmService.update(this.id, this.film).subscribe(res => {
      console.log(res)
      this.film = new Film();
      this.router.navigate([''])
    })
  }

}
