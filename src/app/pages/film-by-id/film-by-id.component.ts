import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Film } from 'src/app/models/Film';
import { FilmService } from 'src/app/services/film.service';

@Component({
  selector: 'app-film-by-id',
  templateUrl: './film-by-id.component.html',
  styleUrls: ['./film-by-id.component.css']
})
export class FilmByIdComponent implements OnInit {

  id!:number;
  film!:Film;

  constructor(private filmService:FilmService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    
    this.filmService.findById(this.id).subscribe(res => {
      this.film = res;
    } ,error => console.log(error))
  }

  deletefilm(): void {
    this.filmService.delete(this.id).subscribe(() =>{
      this.router.navigate([''])
    })
  }

  onSubmit(id:number){
    this.router.navigate(['update',id])
  }

}
