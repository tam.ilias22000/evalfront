import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Film } from 'src/app/models/Film';
import { FilmService } from 'src/app/services/film.service';

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.css']
})
export class FilmListComponent implements OnInit {

  films: Film[] = [];

  constructor(private filmService:FilmService, private router:Router) { }

  ngOnInit(): void {
    this.filmService.findAll().subscribe(res =>{
      this.films = res;
    })
  }

  onSubmit(){
    this.router.navigate(['new'])
  }

  onDeleteFilm(film:Film){

    this.filmService.delete(film.id).subscribe();
    this.films = this.films.filter(unFilm => unFilm.id !== film.id)
  }
}
