import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FilmService } from 'src/app/services/film.service';

@Component({
  selector: 'app-creation',
  templateUrl: './creation.component.html',
  styleUrls: ['./creation.component.css']
})
export class CreationComponent implements OnInit {
  filmCreation!: FormGroup;

  constructor(private formBuilder:FormBuilder, private filmService:FilmService, private router:Router) { 
    this.filmCreation = this.formBuilder.group({
      title: [''],
      synopsis: [''],
      releaseYear: [''],
      director: [''],
      imbdRating: ['']
    })
  }

  ngOnInit(): void {
  }

  onSubmit(){
    this.filmService.save(this.filmCreation.value).subscribe(console.log)
    this.router.navigate([''])
  }

}
