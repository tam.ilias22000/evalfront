export class Film {
    id!:number;
    title!:string;
    synopsis!:string;
    releaseYear!:string;
    director!:string;
    imbdRating!:string;

}