import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Film } from 'src/app/models/Film';

@Component({
  selector: 'app-film-card',
  templateUrl: './film-card.component.html',
  styleUrls: ['./film-card.component.css']
})
export class FilmCardComponent implements OnInit {

  @Input() film!:Film
  @Output() delete = new EventEmitter
  constructor() { }

  ngOnInit(): void {
  }

  onDelete(){
    this.delete.emit();
  }

}
